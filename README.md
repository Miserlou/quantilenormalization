# Quantile Normalize

Given a Pandas dataframe containing microarray data, return the data in quantile normalized form.

## Installation

Clone this repo, `pip install -r requirements.txt`.

## Usage

Given an example input:

    example = pd.DataFrame(
        {   'C1': {'A': 5, 'B': 2, 'C': 3, 'D': 4},
            'C2': {'A': 4, 'B': 1, 'C': 4, 'D': 2},
            'C3': {'A': 3, 'B': 4, 'C': 6, 'D': 8}
        }
    )
    normalized = quantile_normalize(example)
    print(normalized)


## Testing

Testing will test correctness against reference implentations, failure for bad types, and speeds of different random sizes.

`pip install -r test_requirements.txt; ./test.sh`

## Profiling

`python -m cProfile -s cumtime quantile_norm.py`

