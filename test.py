import pandas as pd
import numpy
from random import randint
from nose.tools import assert_raises

from quantile_norm import quantile_normalize

def test_sanity():
    pass

def test_quantile_normalize():
    """ Standard sanity check using Wikipedia data source """
    ex1 = pd.DataFrame(
                    {   'C1': {'A': 5, 'B': 2, 'C': 3, 'D': 4},
                        'C2': {'A': 4, 'B': 1, 'C': 4, 'D': 2},
                        'C3': {'A': 3, 'B': 4, 'C': 6, 'D': 8}
                    }
                )

    normalized = quantile_normalize(ex1)

    assert (normalized.iat[0,0] == 5.666666666666667)
    assert (normalized.iat[0,2] == 2.0)

def test_quantile_normalize_bad():
    """ Make sure the type checker is working """
    ex1 = ['b', 'a', 'd']
    assert_raises(TypeError, quantile_normalize, ex1)


def test_quantile_big():
    """ Randomly generate and test a large frames """

    for range_size in [1, 10, 100, 1000, 10000]:
        frames = {}
        for i in range(range_size):
            col_num = "C" + str(i)
            frames[col_num] = {
                'A': randint(1,10),
                'B': randint(1,10),
                'C': randint(1,10),
                'D': randint(1,10),
            }

        ex2 = pd.DataFrame(frames)
        normalized = quantile_normalize(ex2)

def test_quantile_big_uint():
    """ Randomly generate and test a large frames """

    for range_size in [1, 10, 100, 1000, 10000, 10000]:
        frames = {}
        for i in range(range_size):
            col_num = "C" + str(i)
            frames[col_num] = {
                'A': numpy.uint64(randint(1,10)),
                'B': numpy.uint64(randint(1,10)),
                'C': numpy.uint64(randint(1,10)),
                'D': numpy.uint64(randint(1,10)),
            }

        ex2 = pd.DataFrame(frames)
        normalized = quantile_normalize(ex2)
