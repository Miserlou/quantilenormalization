# -*- coding: utf-8 -*-
#! /usr/bin/python3

"""
Quantile Norm.
Given a Pandas dataframe containing microarray data,
return the data in quantile normalized form.
"""

import numpy as np
import pandas as pd

EXAMPLE_INPUT = pd.DataFrame(
                    {   'C1': {'A': 5, 'B': 2, 'C': 3, 'D': 4},
                        'C2': {'A': 4, 'B': 1, 'C': 4, 'D': 2},
                        'C3': {'A': 3, 'B': 4, 'C': 6, 'D': 8}
                    }
                )

def quantile_normalize(arrays: pd.DataFrame) -> pd.DataFrame:
    """
    Applies Quantile Normalization to a given input.

    Related article:
        https://en.wikipedia.org/wiki/Quantile_normalization

    Reference implementations:
        https://en.wikipedia.org/wiki/Quantile_normalization
        https://stackoverflow.com/a/41078786
        https://github.com/ShawnLYU/Quantile_Normalize/blob/master/quantile_norm.py
        https://pypi.python.org/pypi/microarray_quantilenorm/0.1008 (GPL)
        http://lists.open-bio.org/pipermail/biopython/2010-March/012499.html
        https://support.bioconductor.org/p/57186/
        https://codereview.stackexchange.com/questions/31617/code-correctness-and-refinement-for-quantile-normalization

    Args:
        arrays (pandas.core.frame.DataFrame): Expects a list of lists to be normalized.

    Returns:
        pandas.core.frame.DataFrame: The normalized values.

    """
    if type(arrays) != pd.DataFrame:
        raise TypeError("quantile_normalize expects a pandas DataFrame")

    inputs = arrays.copy()

    # First, rank:
    sorted_inputs = {}
    for column in inputs:
        sorted_inputs.update({column: sorted(inputs[column])})
    sorted_frame = pd.DataFrame(sorted_inputs)
    ranks = sorted_frame.mean(axis=1).tolist() # Using mean, could possibly use median.

    # Then, sort:
    for column in inputs:

        # Sort the invidual columns
        sorted_column = np.sort(inputs[column])

        # Finds indices where elements should be inserted to maintain order.
        ranked_column = np.searchsorted(sorted_column, inputs[column])

        # Set the column to the ranked values
        inputs[column] = [ranks[i] for i in ranked_column]

    return inputs

if __name__ == "__main__":
    normalized = quantile_normalize(EXAMPLE_INPUT)
    print(normalized)
